module.exports = { 
  	host: 'localhost',
  	username: 'root',
  	password: 'root',
  	port: 3306,
	database: 'ejabberd',
	BOSH_SERVICE: 'http://localhost:5280/http-bind/',
	with_server: 'conference.xzy-dell'
}; 