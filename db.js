var mysql = require('mysql'),
    settings = require('./settings');

module.exports = mysql.createPool({
    host : settings.host,
    port : settings.port,
    database : settings.database,
    user : settings.username,
    password : settings.password
});