var app = require('express')();
var xmpp = require('./lib/bosh-prebind');
var http = require('http').Server(app);

var bodyParser = require('body-parser');
var multer = require('multer'); 
var io = require('socket.io')(http);
var settings = require('./settings');
var pool = require('./db.js');

app.use(bodyParser.json()); // for parsing application/json
app.use(bodyParser.urlencoded({ extended: true })); // for parsing application/x-www-form-urlencoded
app.use(multer()); // for parsing multipart/form-data

app.all('*', function(req, res, next) {
    res.header("Access-Control-Allow-Origin", "*");
    res.header("Access-Control-Allow-Headers", "X-Requested-With");
    res.header("Access-Control-Allow-Methods","PUT,POST,GET,DELETE,OPTIONS");
    res.header("X-Powered-By",' 3.2.1')
    res.header("Content-Type", "application/json;charset=utf-8");
    next();
});

app.get('/', function (req, res) {
  res.send('Hello World!');
});

app.post('/prebind', function(req, res){
    console.log(req.body);
    var name = req.body['name'];
    var pwd = req.body['pwd'];
    xmpp.Client(name, pwd, settings.BOSH_SERVICE, 60, 1, function(jid, rid, sid){
    	console.log("rid="+rid);
    	console.log("sid="+sid);
    	var data = {jid: jid.toString(), rid: rid, sid: sid};
    	res.send(data);
    });
});

app.post('/lastmuc', function(req, res){
    var name = req.body['name'];
    pool.getConnection(function (err, conn) {
        if (err) console.log("POOL ==> " + err);
        var sql = 'select distinct with_user as room from archive_collections where with_server = ? and us = ? order by utc desc';
        var argv = [settings.with_server ,name];
        conn.query(sql, argv, function(err,rows){
          if (err) console.log(err);
            res.send(rows);
            conn.release();
        });
    });
});

app.post('/muc', function(req, res){
    var name = req.body['name'];
    pool.getConnection(function (err, conn) {
        if (err) console.log("POOL ==> " + err);
        var sql = 'select distinct MUC_ROOM_NAME as room from MUC where MUC_USER_NAME = ?';
        var argv = [name];
        conn.query(sql, argv, function(err,rows){
          if (err) console.log(err);
            res.send(rows);
            conn.release();
        });
    });
});

app.post('/joinmuc', function(req, res){
    var names = req.body['names'];
    var room = req.body['room'];
    for(var i in names){
        (function (name){
          pool.getConnection(function (err, conn) {
              if (err) console.log("POOL ==> " + err);
              var sql = 'insert into MUC(MUC_USER_NAME, MUC_ROOM_NAME, MUC_CREATE_TIME) values(?, ?, now())';
              var argv = [name, room];
              conn.query(sql, argv, function(err,result){
                if (err) console.log(err);
                  console.log(result.insertId);
                  conn.release();
              });
          });
        })(names[i]);
    }
    res.send('ok');
});

io.on('connection', function (socket) {
    socket.on('invite', function (data) {
      console.log(data);
      for(var i in data['receivers']){
        socket.broadcast.emit(data['receivers'][i], data['room']);
      }
    });

});

http.listen(3000, function () {

  console.log('listening on *:3000');

})


